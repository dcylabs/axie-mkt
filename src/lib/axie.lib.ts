import axios from 'axios';
import rateLimit from 'axios-rate-limit';

const http = rateLimit(axios.create(), { maxRequests: 5, perMilliseconds: 1500 })

export const WETH_DIVISOR = 1000000000000000000;

export async function loadAxiesList(): Promise<any> {
  return [
    ...(await loadAxiesFromCriterias([], ["back-kingfisher", "mouth-peace-maker", "horn-wing-horn", "tail-swallow", "tail-snowy-swallow"])),
    ...(await loadAxiesFromCriterias([], ["back-green-thorns", "mouth-dango", "mouth-tiny-turtle", "mouth-tiny-carrot", "horn-incisor", "tail-iguana", "tail-fir-trunk", "tail-tiny-dino"])),
    ...(await loadAxiesFromCriterias([], ["back-pumpkin","mouth-humorless","mouth-serious","tail-hot-butt","tail-carrot","horn-leaf-bug"])),
    ...(await loadAxiesFromCriterias([], [])),
    ...(await loadAxiesFromCriterias(["Plant"], [])),
  ];
}

export async function loadAxiesFromCriterias(classes = [], parts = []): Promise<any> {
  return new Promise((resolve) => {
    http.post("https://axieinfinity.com/graphql-server-v2/graphql",
        {
        "operationName":"GetAxieBriefList",
        "variables":{
            "from":0,
            "size":24,
            "sort":"PriceAsc",
            "auctionType":"Sale",
            "owner":null,
            "criteria":{
                "region":null,
                "parts":parts,
                "bodyShapes":null,
                "classes":classes,
                "stages":null,
                "numMystic":null,
                "pureness":null,
                "title":null,
                "breedable":null,
                "breedCount":null,
                "hp":[

                ],
                "skill":[

                ],
                "speed":[

                ],
                "morale":[

                ]
            }
        },
        "query":"query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieBrief\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieBrief on Axie {\n  id\n  name\n  stage\n  class\n  breedCount\n  image\n  title\n  battleInfo {\n    banned\n    __typename\n  }\n  auction {\n    currentPrice\n    currentPriceUSD\n    __typename\n  }\n  parts {\n    id\n    name\n    class\n    type\n    specialGenes\n    __typename\n  }\n  __typename\n}\n"
        }
      )
    .then(function(response) {
      return response.data;
    })
    .then(function(json) {
      resolve(json.data.axies.results.map((it) => it.id))
      console.log(json.data.axies.results);
    });
  })
}

export async function getDetails(axieID: string): Promise<any> {
    return new Promise((resolve) => {
      http.post("https://axieinfinity.com/graphql-server-v2/graphql",
            {
            "operationName":"GetAxieDetail",
            "variables":{
                "axieId":axieID
            },
            "query":"query GetAxieDetail($axieId: ID!) {\n  axie(axieId: $axieId) {\n    ...AxieDetail\n    __typename\n  }\n}\n\nfragment AxieDetail on Axie {\n  id\n  image\n  class\n  chain\n  name\n  genes\n  owner\n  birthDate\n  bodyShape\n  class\n  sireId\n  sireClass\n  matronId\n  matronClass\n  stage\n  title\n  breedCount\n  level\n  figure {\n    atlas\n    model\n    image\n    __typename\n  }\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  ownerProfile {\n    name\n    __typename\n  }\n  battleInfo {\n    ...AxieBattleInfo\n    __typename\n  }\n  children {\n    id\n    name\n    class\n    image\n    title\n    stage\n    __typename\n  }\n  __typename\n}\n\nfragment AxieBattleInfo on AxieBattleInfo {\n  banned\n  banUntil\n  level\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
            })
        .then(function(response) {
          return response.data;
        })
        .then(function(json) {
          resolve(json.data.axie);
        });
      })
  }
