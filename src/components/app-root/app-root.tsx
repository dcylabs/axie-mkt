import { Component, h, State } from '@stencil/core';
import { loadAxiesList } from '../../lib/axie.lib';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.scss',
  shadow: true,
})
export class AppRoot {

  @State()
  axies = [];

  componentDidLoad(): void {
    this.loadAxies(); 
  }

  async loadAxies(): Promise<void> {
    this.axies = await loadAxiesList();
    console.log(this.axies);
  }

  render() {
    return (
      <div>
        { 
          this.axies.map((it) => {
            return <axie-preview axieID={it}></axie-preview>
          })
        }
      </div>
    );
  }
}
