import { Component, h, Prop, State } from '@stencil/core';
import { getDetails, WETH_DIVISOR } from '../../lib/axie.lib';
import * as hdate from 'human-date';

@Component({
  tag: 'axie-preview',
  styleUrl: 'axie-preview.scss',
  shadow: true,
})
export class AxiePreview {

  @Prop()
  axieID: string;

  @State() 
  axieDetails: any;

  componentWillLoad(): void { 
    this.loadData();    
  }

  async loadData(): Promise<void> {
    this.axieDetails = await getDetails(this.axieID);
  }

  renderLoader() {
    return <div onClick={() => this.loadData()}> Loading axie #{this.axieID} </div>
  }

  renderAuction() {
    if(this.axieDetails.auction == null) {
      return <div class="auction-ended">
        Ended 
      </div>
    }


    const auctionTime = this.axieDetails.auction.endingTimestamp - this.axieDetails.auction.startingTimestamp;
    const remainingTime = this.axieDetails.auction.endingTimestamp - (new Date().getTime() / 1000);
    const percentageTime = 1 - (remainingTime / auctionTime); 
    
    if(remainingTime < 0) {
      return <div class="auction-ended">
        Ended 
      </div>
    }

    return <div class="auction-running">
      <div class="prices">
        <div class="current">{ Math.round((this.axieDetails.auction.currentPrice / WETH_DIVISOR) * 10000) / 10000 }</div>
        <div class="start">{ Math.round((this.axieDetails.auction.startingPrice / WETH_DIVISOR) * 10000) / 10000 }</div>
        <div class="end">{ Math.round((this.axieDetails.auction.endingPrice / WETH_DIVISOR) * 10000) / 10000 }</div>
      </div>
      <div class="duration-progress">
        <div class="bar-holder">
          <div class="bar" style={{'width': (percentageTime * 100) + "%"}}></div>
        </div>
        <div class="legend">
          {
            hdate.relativeTime(remainingTime)
          }
        </div>
      </div> 
    </div>
  }

  renderAxie() {
    const marketplaceURL = `https://marketplace.axieinfinity.com/axie/${this.axieID}`;
    const classes = [
      'axie', 
      'axie-' + this.axieDetails.class?.toLowerCase()
    ].join(' ');
    return <a class={classes} href={marketplaceURL} target="_blank">
      <div class="class">
        {this.axieDetails.class}
      </div>
      <div class="breed-count">
        { this.axieDetails.breedCount } / 7 
      </div>
      <div style={{'height': '20px'}}></div>
      <div class="visual" style={{'background-image': 'url(' + this.axieDetails.image + ')'}}>
      </div>
      <div class="parts">
        { this.axieDetails.parts.map((it) => {
          return <div class={'part part-' + it.class.toLowerCase()}> {it.id} </div>
        })}
      </div>
       { this.renderAuction() }                              
    </a>
  }

  render() {
    return (
      <div class="axie-preview">
        { 
          this.axieDetails == null ? 
          this.renderLoader() : 
          this.renderAxie() 
        }
      </div>
    );
  }
}
